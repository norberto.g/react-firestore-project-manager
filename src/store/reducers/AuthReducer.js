const initState={
    authError:null
}
const AuthReducer=(state=initState,action) =>{
    switch(action.type){
        case 'LOGIN_SUCCESS':
            console.log("Login success");    
            return {
                ...state,
                authError:null
            }
        case 'LOGIN_ERROR':
            console.log("Login Error");    
            return {
                ...state,
                authError:'login failed!'
            }
        case 'LOGOUT_SUCCESS':
            console.log("Logout success");    
            return state;
        case 'LOGOUT_ERROR':
            console.log("Logout Error");    
            return state;
        case 'SIGNUP_SUCCESS':
            console.log("Signup success");    
            return {
                ...state,
                authError:null
            }
        case 'SIGNUP_ERROR':
            console.log("Signup Error");    
            return {
                ...state,
                authError:action.err.message
            };    
        default:
            return state;

    }
    
} 

export default AuthReducer;