const initState={
    activePage:1,
    itemsCountPerPage:5,
    totalItemsCount:10,
    pageRangeDisplayed:5,
    projects:[]
}
const ProjectReducer=(state=initState,action) =>{
    switch(action.type){
        case 'CREATE_PROJECT':
            console.log("created project ",action.project);
            return state;
        
        case 'CREATE_PROJECT_ERROR':
            console.log("Error al guardar el proyecto", action.err);
            return state;
        case 'CHANGE_PAGE':
            const newState={
                ...state
            }
            newState.activePage=action.pageNumber;
            return newState;
        default:
            return state;
        

    }


    
    
} 

export default ProjectReducer;