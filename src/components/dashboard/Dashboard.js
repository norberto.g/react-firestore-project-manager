import React, { Component } from 'react';
import Notifications from './Notifications';
import ProjectList from '../projects/ProjectList';
import {connect} from 'react-redux';
import {firestoreConnect} from 'react-redux-firebase';
import {compose} from 'redux';
import {Redirect} from 'react-router-dom';
import Pagination from "react-js-pagination";
import {handlePageChange} from '../../store/actions/projectActions';

class Dashboard extends Component {
    render() {
        const handlePageChange=(pageNumber)=> {
            this.props.handlePageChange(pageNumber)
        }
        const {projects,auth,notifications,itemCount,activePage,itemsCountPerPage} = this.props;
        if(!auth.uid)
            return <Redirect to='/signin'/>
        return (
            <div className="dashboard container">
                <div className="row">
                    <div className="col s12 m6">
                        <ProjectList projects={projects} page={activePage} rowsPerPage={itemsCountPerPage}/>
                    </div>
                    <div className="col s12 m5 offset-m1">
                        <Notifications notifications={notifications} />
                    </div>
                    <Pagination activePage={activePage} itemsCountPerPage={itemsCountPerPage} totalItemsCount={itemCount}
                        pageRangeDisplayed={5}  onChange={handlePageChange}
        />
                </div>
            </div>
        );
    }
}

const mapStateToProps=(state)=>{
    return {
        projects:state.firestore.ordered.projects,
        auth:state.firebase.auth,
        notifications:state.firestore.ordered.notifications,
        itemCount:state.firestore.ordered.projects ? state.firestore.ordered.projects.length:0,
        activePage:state.project.activePage,
        itemsCountPerPage:state.project.itemsCountPerPage

    }
}

const mapDispatchToProps=(dispatch)=>{
    return {
        handlePageChange:(pageNumber)=>dispatch(handlePageChange(pageNumber))
    }
}

export default compose(
    connect(mapStateToProps,mapDispatchToProps),
    firestoreConnect([
        {collection:'projects',orderBy:['createdAt','desc']},
        {collection:'notifications',limit: 3,orderBy:['time','desc']}
    ])
)(Dashboard);