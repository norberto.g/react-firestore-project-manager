import React, { Component } from 'react';
import ProjectSummary from './ProjectSummary';
import {Link} from 'react-router-dom';


class ProjectList extends Component {
    render(){
        const {projects}=this.props;
        const offset=this.props.page==1? 0:(((this.props.page-1)*this.props.rowsPerPage))+1;
        const limit=offset+this.props.rowsPerPage;
        return (
            <div  className="project-list section">
                {projects && projects.filter((item,i)=>{
                    if(i>=offset && i<=limit){
                        return true;
                    }
                }).map(project=>{
                    return (
                        <Link to={'project/' + project.id} key={project.id}>  
                            <ProjectSummary project={project} />
                        </Link>
                    )
                })}
            </div>
        
        
        );
    }
}; 
export default ProjectList;