import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';
// Initialize Firebase
 var config = {
    apiKey: "AIzaSyD7sDFdPw94P10Kdwcqs9Ro_WQVziCXVj4",
    authDomain: "projectmanager-ce025.firebaseapp.com",
    databaseURL: "https://projectmanager-ce025.firebaseio.com",
    projectId: "projectmanager-ce025",
    storageBucket: "projectmanager-ce025.appspot.com",
    messagingSenderId: "775613201429"
  };
  firebase.initializeApp(config);

  firebase.firestore().settings({timestampsInSnapshots:true});


  export default firebase;