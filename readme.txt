Third party libraries used in project

"firebase": "^5.7.0",
"react-redux": "^6.0.0", must downgrade to 5.1.1
"react-redux-firebase": "^2.2.4",
"react-router-dom": "^4.3.1",
"redux": "^4.0.1",
"redux-firestore": "^0.6.0",
"redux-thunk": "^2.3.0"

Thunk is a middleware for creating store actions 
Redux Firebase is library for connecting to firebase
Redux Firestore is used to communicate with database
Compose is used to use firebase and firestore for the store at the same time

firebase-tools installed for cloud functions
-login
-firebase init
-keep follow wizard

